import React from 'react';
import Helmet from 'react-helmet';
import Page from '../components/Page';
import getImageMime from '../utils/getImageMime';

export default function Template({
  data // this prop will be injected by the GraphQL query we'll write in a bit
}) {
  const { markdownRemark: post } = data; // data.markdownRemark holds our post data
  const image = post.frontmatter.image && post.frontmatter.image.childImageSharp;
  return (
    <div className="container">
      <Helmet>
        <title>{`${post.frontmatter.page_title} - Sävedalsloppet`}</title>
        <meta name="og:url" content={post.frontmatter.path} />
        <meta name="description" content={post.frontmatter.description} />
        <meta name="og:description" content={post.frontmatter.description} />
        <meta name="og:image" content={image.resize.src} />
        <meta name="og:image_type" content={getImageMime(image.resize.src)} />
      </Helmet>
      <div className="right">
        <div className="image">
          <div
            style={{
              paddingBottom: `${1 / image.responsiveSizes.aspectRatio * 100}%`,
              position: `relative`,
              width: `100%`,
              bottom: 0,
              left: 0,
              backgroundImage: `url(${image.responsiveSizes.base64})`,
              backgroundSize: `cover`,
            }}
          />
          <img
            src={image.responsiveSizes.src}
            srcSet={image.responsiveSizes.srcSet}
            style={{
              width: `100%`,
              height: `100%`,
              margin: 0,
              verticalAlign: `middle`,
              objectFit: 'cover',
              position: 'absolute',
              top: 0,
              left: 0,
            }}
            sizes={image.responsiveSizes.sizes}
          />
        </div>
      </div>
      <div className="left">
        <Page>
          <article>
            <h1>{post.frontmatter.title}</h1>
            <h2>{post.frontmatter.subtitle}</h2>
            <div className="lead"><strong>{post.frontmatter.summary}</strong></div>
            <div className="cols" dangerouslySetInnerHTML={{ __html: post.html }} />
          </article>
        </Page>
      </div>
    </div>
  );
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        subtitle
        page_title
        description
        image {
          childImageSharp {
            resize(
              width:476
              height:249
            ) {
              src
            }
            responsiveSizes(
              toFormat: PNG
            ) {
              base64
              aspectRatio
              src
              srcSet
              sizes
              originalImg
              originalName
            }
          }
        }
      }
    }
  }
`
;
