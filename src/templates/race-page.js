import React from 'react';
import Helmet from 'react-helmet';
import Page from '../components/Page';
import SubMenu from '../components/SubMenu';
import MapCanvas from '../components/MapCanvas';
import getImageMime from '../utils/getImageMime';

export default function Template({
  data,
}) {
  const { markdownRemark: post } = data; // data.markdownRemark holds our post data
  const image = post.frontmatter.image && post.frontmatter.image.childImageSharp;
  const imageRight = image && image.responsiveSizes;
  return (
    <div className="container">
      <Helmet>
        <title>{`${post.frontmatter.page_title} - Sävedalsloppet`}</title>
        <meta name="og:url" content={post.frontmatter.path} />
        <meta name="description" content={post.frontmatter.description} />
        <meta name="og:description" content={post.frontmatter.description} />
        <meta name="og:image" content={image.resize.src} />
        <meta name="og:image_type" content={getImageMime(image.resize.src)} />
      </Helmet>
      <div className="right">
        {post.frontmatter.map && (
          <MapCanvas selected={post.frontmatter.map} />
        )}
        {!post.frontmatter.map && post.frontmatter.image && (
          <div className="image">
            <div
              style={{
                paddingBottom: `${1 / imageRight.aspectRatio * 100}%`,
                position: `relative`,
                width: `100%`,
                bottom: 0,
                left: 0,
                backgroundImage: `url(${imageRight.base64})`,
                backgroundSize: `cover`,
              }}
            />
            <img
              src={imageRight.src}
              srcSet={imageRight.srcSet}
              style={{
                width: `100%`,
                height: `100%`,
                margin: 0,
                verticalAlign: `middle`,
                objectFit: 'cover',
                position: 'absolute',
                top: 0,
                left: 0,
              }}
              sizes={imageRight.sizes}
            />
          </div>
        )}
      </div>
      <div className="left">
        <Page>
          <SubMenu />
          <article>
            <h1>{post.frontmatter.title}</h1>
            <h2 style={{ marginBottom: 16 }}>{post.frontmatter.subtitle}</h2>
            {post.frontmatter.map && post.frontmatter.image && (
              <div style={{ marginBottom: 16 }}>
                {image && (
                  <div
                    className="img"
                    style={{
                      paddingBottom: `${1 / image.responsiveSizes.aspectRatio * 100}%`,
                      position: `relative`,
                      width: `100%`,
                      bottom: 0,
                      left: 0,
                      backgroundImage: `url(${image.responsiveSizes.base64})`,
                      backgroundSize: `cover`,
                    }}
                  >
                    <img
                      src={image.responsiveSizes.src}
                      srcSet={image.responsiveSizes.srcSet}
                      style={{
                        width: `100%`,
                        margin: 0,
                        verticalAlign: `middle`,
                        position: `absolute`,
                      }}
                      sizes={image.responsiveSizes.sizes}
                      alt={post.frontmatter.title}
                    />
                  </div>
                )}
              </div>
            )}
            <div className="tc">
              <p><a href="/om-loppet">Se hela programmet</a> med alla starttider, klasser, avgifter och banor.</p>
            </div>
            <div className="cols" dangerouslySetInnerHTML={{ __html: post.html }} />
          </article>
          <section>
            <div className="articles clearfix">
              <article>
                <div className="article super action">
                  <h3><a href="/startlista">Visa deltagare</a></h3>
                </div>
              </article>
              <article>
                <div className="article super action">
                  <h3><a href="/anmalan">Anmäl dig nu</a></h3>
                </div>
              </article>
            </div>
          </section>
        </Page>
      </div>
    </div>
  );
}

export const pageQuery = graphql`
  query RacePageByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date
        path
        title
        subtitle
        page_title
        description
        map
        image {
          childImageSharp {
            resize(
              width:476
              height:249
            ) {
              src
            }
            responsiveSizes(
              toFormat: PNG
            ) {
              base64
              aspectRatio
              src
              srcSet
              sizes
              originalImg
              originalName
            }
          }
        }
      }
    }
  }
`
;
