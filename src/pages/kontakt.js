import React from 'react';
import Helmet from 'react-helmet';
import Page from '../components/Page';

class IndexPage extends React.PureComponent {
  render() {
    const imageRight = this.props.data.imageRight.responsiveSizes;
    return (
      <div className="container">
        <Helmet>
          <title>{`Kontakt - Sävedalsloppet`}</title>
          <meta name="og:url" content="/kontakt" />
          <meta name="description" content="Har du några frågor eller funderingar, tveka inte att höra av dig till oss." />
          <meta name="og:description" content="Har du några frågor eller funderingar, tveka inte att höra av dig till oss." />
          <meta name="og:image" content="/IMG_2914.jpg" />
          <meta name="og:image_type" content="image/jpeg" />
        </Helmet>
        <div className="right">
          <div className="image">
            <div
              style={{
                paddingBottom: `${1 / imageRight.aspectRatio * 100}%`,
                position: `relative`,
                width: `100%`,
                bottom: 0,
                left: 0,
                backgroundImage: `url(${imageRight.base64})`,
                backgroundSize: `cover`,
              }}
            />
            <img
              src={imageRight.src}
              srcSet={imageRight.srcSet}
              style={{
                width: `100%`,
                height: `100%`,
                margin: 0,
                verticalAlign: `middle`,
                objectFit: 'cover',
                position: 'absolute',
                top: 0,
                left: 0,
              }}
              sizes={imageRight.sizes}
            />
          </div>
        </div>
        <div className="left">
          <Page>
            <div className="contact">
              <h1>Kontakt</h1>
              <h2>Har du några frågor eller funderingar, tveka inte att höra av dig till oss.</h2>
              <div className="tc" style={{ paddingTop: 20 }}>
                <div style={{ marginBottom: 20 }}>
                  <dl>
                    <dd><strong>Sävedalens AIK</strong></dd>
                    <dd>Friidrottssektionen</dd>
                    <dd>Box 2020</dd>
                    <dd>433 03 Sävedalen</dd>
                  </dl>
                </div>
                <div style={{ marginBottom: 20 }}>
                  <dl>
                    <dd><strong>Vallhamra Idrottsplats</strong></dd>
                    <dd>Hultvägen</dd>
                    <dd>433 64 Sävedalen</dd>
                    <dd><a href="https://www.google.se/maps/place/Idrottsplats+Vallhamra/@57.724374,12.078076,17z/data=!3m1!4b1!4m2!3m1!1s0x464ff6a1a6301ba3:0x53dd1404952fbc02?hl=sv" target="_blank"><strong>Hitta hit med karta</strong></a></dd>
                    <dd>--</dd>
                    <dd>Öppet Måndag till Fredag</dd>
                    <dd>Mellan kl. 09.00 - 14.00 (prel. tider)</dd>
                  </dl>
                </div>
                <div style={{ marginBottom: 20 }}>
                  <dl>
                    <dd><strong>Mats Erixon (kansli)</strong></dd>
                    <dd><strong>Telefon</strong></dd>
                    <dd><a href="tel:+46702152945">0702 - 15 29 45</a></dd>
                    <dd><strong>E-post</strong></dd>
                    <dd><a href="mailto:savedalsloppet@saik.nu">savedalsloppet@saik.nu</a></dd>
                  </dl>
                </div>
              </div>
            </div>
          </Page>
        </div>
      </div>
    );
  }
}

export default IndexPage

export const pageQuery = graphql`
  query ContactQuery {
    imageRight: imageSharp(id: { regex: "/\/IMG_2914.jpg/" }) {
      responsiveSizes(
        toFormat: PNG
      ) {
        base64
        aspectRatio
        src
        srcSet
        sizes
        originalImg
        originalName
      }
    }
  }
`
