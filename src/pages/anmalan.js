import React from 'react'
import Link, { navigateTo } from 'gatsby-link'
import moment from 'moment'
import 'moment/locale/sv';
import Helmet from 'react-helmet';
import Page from '../components/Page';

moment.locale('sv');

class IndexPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.lastReg = '2017-10-17T09:00+0200';
    this.lastAfterReg = '2017-10-21T09:00+0200';
    this.state = this.calculateDateParts();
  }

  calculateDateParts() {
    const now = moment();
    let diff = moment(this.lastReg).diff(now) / 1000;
    const dateParts = { afterReg: false };
    if (diff <= 0) {
      diff = moment(this.lastAfterReg).diff(now) / 1000;
      dateParts.afterReg = true;
    }
    const days = Math.floor(diff/3600/24);
    const hours = Math.floor(diff/3600 % 24);
    const minutes = Math.floor(diff/60 % 60);
    const seconds = Math.floor(diff % 60);

    dateParts.ended = true;
    if (days > 0) {
      dateParts.days = days;
      dateParts.ended = false;
    }
    if (hours >= 0) {
      dateParts.hours = hours;
      dateParts.ended = false;
    }
    if (minutes >= 0) {
      dateParts.minutes = minutes;
      dateParts.ended = false;
    }
    if (seconds >= 0) {
      dateParts.seconds = seconds;
      dateParts.ended = false;
    }
    return dateParts;
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      var dateParts = this.calculateDateParts();
      this.setState(dateParts);
    }, 500);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const imageRight = this.props.data.imageRight.responsiveSizes;
    return (
      <div className="container">
        <Helmet>
          <title>{`Anmälan - Sävedalsloppet`}</title>
          <meta name="og:url" content="/anmalan" />
          <meta name="description" content="Du anmäler dig till Sävedalsloppet direkt här på hemsidan. Anmälan är öppen fram till måndag den 16 oktober för Sävedalsloppet 2017." />
          <meta name="og:description" content="Du anmäler dig till Sävedalsloppet direkt här på hemsidan. Anmälan är öppen fram till måndag den 16 oktober för Sävedalsloppet 2017." />
          <meta name="og:image" content="/SavedalsloppetDSC_0220b.jpg" />
          <meta name="og:image_type" content="image/jpeg" />
        </Helmet>
        <div className="right">
          <div className="image">
            <div
              style={{
                paddingBottom: `${1 / imageRight.aspectRatio * 100}%`,
                position: `relative`,
                width: `100%`,
                bottom: 0,
                left: 0,
                backgroundImage: `url(${imageRight.base64})`,
                backgroundSize: `cover`,
              }}
            />
            <img
              src={imageRight.src}
              srcSet={imageRight.srcSet}
              style={{
                width: `100%`,
                height: `100%`,
                margin: 0,
                verticalAlign: `middle`,
                objectFit: 'cover',
                position: 'absolute',
                top: 0,
                left: 0,
              }}
              sizes={imageRight.sizes}
            />
          </div>
        </div>
        <div className="left">
          <Page>
            <div className="register">
              {!this.state.ended ? (
                <section>
                  <h1>Anmälan</h1>
                  <div className="cols">
                    <p>
                      <strong>Bra att veta vid anmälan</strong>
                      <i>Här hittar du information om anmälan till Sävedalsloppet 2017.</i>
                    </p>
                    <p>
                      <strong>Att anmäla sig</strong>
                      Du anmäler dig till Sävedalsloppet genom vår externa partner länkad här nedanför på hemsidan.
                    </p>
                    <p>
                      Sista anmälningsdag (till ordinarie pris) är måndagen den 16 oktober 2017 till midnatt kl 24:00.
                    </p>
                    <p>
                      <strong>Efteranmälan</strong>
                      Efteranmälan kan ske på anmälningssidan från tisdagen den 17 oktober till fredagen den 20 oktober kl 12.00. Anmälan kan därefter ske på tävlingsdagen från kl 9 i tävlingscentrum.
                    </p>
                    <p style={{color: 'red'}}>
                      Efter det sker efteranmälan på Vallhamra Idrottsplats på tävlingsdagen vid tävlingscentrum mot en extra avgift på 100 kronor och 50kr/deltagare i ungdomsklasserna.
                    </p>
                    <p>
                      <strong>Anmälningsavgift 2017</strong>
                      Anmälningsavgifter för respektive lopp och klass enligt följande:
                    </p>
                    <ul>
                      <li>10km – 250 kronor</li>
                      <li>5km – 200 kronor</li>
                      <li>3000m hinder – 150 kronor (främst för motionärer)</li>
                      <li>2.5km – 120 kronor</li>
                      <li>1km – 100 kronor</li>
                      <li>Knatteklassen – 50 kronor</li>
                    </ul>
                    <p>
                      <strong>Om loppet</strong>
                      Om du vill läsa mer information inför ditt deltagande i loppet, <a href="/om-loppet">klicka här</a>.
                    </p>
                    <p>
                      <strong>Kontakta oss</strong>
                      Om du har frågor kring din anmälan, kontakta Sävedalens AIK via e-post:<br />
                      <a href="mailto:savedalsloppet@saik.nu">kansli@saik.nu</a>.
                    </p>
                  </div>

                  <section>
                    <div className="articles clearfix">
                      <article style={{width: '100%'}}>
                        <div className="article super action">
                          {this.state.afterReg ? (
                            <h3><a href="https://signup.eqtiming.no/?Event=Savedalslopp&lang=swedish" target="_blank">Till efteranmälan</a></h3>
                          ):(
                            <h3><a href="https://signup.eqtiming.no/?Event=Savedalslopp&lang=swedish" target="_blank">Till anmälan</a></h3>
                          )}
                        </div>
                      </article>
                    </div>
                  </section>

                  {this.state.afterReg ? (
                    <h2>Tid kvar tills efteranmälan stänger</h2>
                  ):(
                    <h2>Tid kvar tills anmälan stänger</h2>
                  )}

                  <aside style={{marginTop: '16px'}} className="countdown with-time no-minutes" id="countdown">
                    {this.state.days ? (
                      <div className="countdown-item countdown-days">
                        <span className="countdown-caption countdown-caption-days">Dagar</span>
                        <span className="countdown-value countdown-value-days">
                          {this.state.days}
                        </span>
                      </div>
                    ):''}
                      {this.state.days > 0 || this.state.hours > 0 ? (
                      <div className="countdown-item countdown-hours">
                        <span className="countdown-caption countdown-caption-hours">Timmar</span>
                        <span className="countdown-value countdown-value-hours">
                          {this.state.hours}
                        </span>
                      </div>
                    ):''}
                    <div className="countdown-item countdown-minutes">
                      <span className="countdown-caption countdown-caption-minutes">Minuter</span>
                      <span className="countdown-value countdown-value-minutes">
                        {this.state.minutes}
                      </span>
                    </div>
                    <div className="countdown-item countdown-seconds">
                      <span className="countdown-caption countdown-caption-seconds">Sekunder</span>
                      <span className="countdown-value countdown-value-seconds">
                        {this.state.seconds}
                      </span>
                    </div>
                  </aside>
                  <div className="tc">
                    <p><Link href="/om-loppet">Se hela programmet</Link> med alla starttider, klasser, avgifter och banor.</p>
                  </div>
                </section>
              ):(
                <section>
                  <h1>Anmälan stängd</h1>
                  <div className="cols">
                    <p>
                      <strong>Bra att veta vid anmälan</strong>
                      <i>Här hittar du information om anmälan till Sävedalsloppet 2017.</i>
                    </p>
                    <p>
                      <strong>Att anmäla sig</strong>
                      Du anmäler dig till Sävedalsloppet genom vår externa partner länkad här nedanför på hemsidan.
                    </p>
                    <p>
                      Sista anmälningsdag (till ordinarie pris) är måndagen den 16 oktober 2017 till midnatt kl 24:00.
                    </p>
                    <p>
                      <strong>Efteranmälan</strong>
                      Efteranmälan kan ske på anmälningssidan från tisdagen den 17 oktober till fredagen den 20 oktober kl 12.00. Anmälan kan därefter ske på tävlingsdagen från kl 9 i tävlingscentrum.
                    </p>
                    <p style={{color: 'red'}}>
                      Efter det sker efteranmälan på Vallhamra Idrottsplats på tävlingsdagen vid tävlingscentrum mot en extra avgift på 100 kronor och 50kr/deltagare i ungdomsklasserna.
                    </p>
                    <p>
                      <strong>Anmälningsavgift 2017</strong>
                      Anmälningsavgifter för respektive lopp och klass enligt följande:
                      <ul>
                        <li>10km – 250 kronor</li>
                        <li>5km – 200 kronor</li>
                        <li>3000m hinder – 150 kronor (främst för motionärer)</li>
                        <li>2.5km – 120 kronor</li>
                        <li>1km – 100 kronor</li>
                        <li>Knatteklassen – 50 kronor</li>
                      </ul>
                    </p>
                    <p>
                      <strong>Om loppet</strong>
                      Om du vill läsa mer information inför ditt deltagande i loppet, <Link to="/om-loppet">klicka här</Link>.
                    </p>
                    <p>
                      <strong>Kontakta oss</strong>
                      Om du har frågor kring din anmälan, kontakta Sävedalens AIK via e-post:<br />
                      <a href="mailto:savedalsloppet@saik.nu">kansli@saik.nu</a>.
                    </p>
                  </div>

                  <section>
                    <div className="articles clearfix">
                      <article style={{width: '100%'}}>
                        <div className="article super action">
                          <h3><Link to="/resultat">Till resultat</Link></h3>
                        </div>
                      </article>
                    </div>
                  </section>
                </section>
              )}
            </div>
          </Page>
        </div>
      </div>
    );
  }
}

export default IndexPage

export const pageQuery = graphql`
  query RegisterQuery {
    imageRight: imageSharp(id: { regex: "/\/SavedalsloppetDSC_0220b.jpg/" }) {
      responsiveSizes(
        toFormat: PNG
      ) {
        base64
        aspectRatio
        src
        srcSet
        sizes
        originalImg
        originalName
      }
    }
  }
`
