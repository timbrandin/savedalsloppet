import React from 'react';
import Link, { navigateTo } from 'gatsby-link';
import Helmet from 'react-helmet';
import moment from 'moment';
import 'moment/locale/sv';
import Page from '../components/Page';

moment.locale('sv');

export default function Index({
  data
}) {
  const { edges: posts } = data.allMarkdownRemark;
  return (
    <div className="container">
      <Helmet>
        <title>{`Aktuellt - Sävedalsloppet`}</title>
        <meta name="og:url" content="/aktuellt" />
        <meta name="description" content="Här kan du läsa om allt aktuellt som händer kring Sävedalsloppet." />
        <meta name="og:description" content="Här kan du läsa om allt aktuellt som händer kring Sävedalsloppet." />
        <meta name="og:image" content="/SavedalsloppetDSC_0371b.jpg" />
        <meta name="og:image_type" content="image/jpeg" />
      </Helmet>
      <div className="right">
        <div className="image" style={{backgroundImage: `url(/SavedalsloppetDSC_0371b.jpg)`}}></div>
      </div>
      <div className="left">
        <Page>
          <div className="blog">
              <h1>Aktuellt för tävlingen</h1>
              <h2>Här kan du läsa om saker som händer kring Sävedalsloppet</h2>

              <div className="articles clearfix">
                {posts
                  .filter(post => post.node.frontmatter.title.length > 0)
                  .map(({ node: post }) => {
                    const responsiveSizes = post.frontmatter.image.childImageSharp.responsiveSizes || {};
                    return (
                      <article key={post.id}>
                        <div className="article" onClick={() => navigateTo(post.frontmatter.path)}>
                          {post.frontmatter.image && (
                            <Link to={post.frontmatter.path}>
                              <div
                                className="img"
                                style={{
                                  paddingBottom: `${1 / responsiveSizes.aspectRatio * 100}%`,
                                  position: `relative`,
                                  width: `100%`,
                                  bottom: 0,
                                  left: 0,
                                  backgroundImage: `url(${responsiveSizes.base64})`,
                                  backgroundSize: `cover`,
                                }}
                              >
                                <img
                                  src={responsiveSizes.src}
                                  srcSet={responsiveSizes.srcSet}
                                  style={{
                                    width: `100%`,
                                    margin: 0,
                                    verticalAlign: `middle`,
                                    position: `absolute`,
                                  }}
                                  sizes={responsiveSizes.sizes}
                                  alt={post.frontmatter.title}
                                />
                              </div>
                            </Link>
                          )}
                          <h2>
                            <Link to={post.frontmatter.path}>
                              {post.frontmatter.title}
                            </Link>
                          </h2>
                          <div
                            style={{
                              marginBottom: 4,
                              color: '#999',
                              fontSize: 12,
                            }}
                          >{moment(post.frontmatter.date).format('[Uppdaterad: ]DD MMMM YYYY')}</div>
                          <p>{post.frontmatter.summary || post.excerpt}</p>
                        </div>
                      </article>
                    );
                })}
              </div>
            </div>
          </Page>
        </div>
      </div>
  );
}

export const pageQuery = graphql`
  query BlogQuery {
    allMarkdownRemark(
      filter: { id: { regex: "/\/aktuellt\//" } }
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      edges {
        node {
          excerpt(pruneLength: 250)
          id
          frontmatter {
            title
            date
            path
            image {
              childImageSharp {
                responsiveSizes(
                  maxWidth: 400
                  toFormat: PNG
                ) {
                  base64
                  aspectRatio
                  src
                  srcSet
                  sizes
                  originalImg
                  originalName
                }
              }
            }
            summary
          }
        }
      }
    }
    imageRight: imageSharp(id: { regex: "/\/SavedalsloppetDSC_0371b.jpg/" }) {
      responsiveSizes(
        toFormat: PNG
      ) {
        base64
        aspectRatio
        src
        srcSet
        sizes
        originalImg
        originalName
      }
    }
  }
`;
