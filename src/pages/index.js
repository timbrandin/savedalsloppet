import React from 'react'
import Link, { navigateTo } from 'gatsby-link'
import moment from 'moment'
import 'moment/locale/sv';
import Helmet from 'react-helmet';
import Page from '../components/Page';

moment.locale('sv');

class IndexPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.datetime = '2017-10-21T11:00+0200';
    this.state = {
      ...this.calculateDateParts(),
    };
  }

  calculateDateParts() {
    const now = moment();
    const diff = moment(this.datetime).diff(now) / 1000;

    const days = Math.floor(diff/3600/24);
    const hours = Math.floor(diff/3600 % 24);
    const minutes = Math.floor(diff/60 % 60);
    const seconds = Math.floor(diff % 60);
    const dateParts = {};

    dateParts.ended = true;
    if (days > 0) {
      dateParts.days = days;
      dateParts.ended = false;
    }
    if (hours >= 0) {
      dateParts.hours = hours;
      dateParts.ended = false;
    }
    if (minutes >= 0) {
      dateParts.minutes = minutes;
      dateParts.ended = false;
    }
    if (seconds >= 0) {
      dateParts.seconds = seconds;
      dateParts.ended = false;
    }
    return dateParts;
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      var dateParts = this.calculateDateParts();
      this.setState(dateParts);
    }, 500);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const imageRight = this.props.data.imageRight.responsiveSizes;
    return (
      <div className="container front">
        <Helmet>
          <title>{`Sävedalsloppet 2017`}</title>
          <meta name="og:title" content={`Sävedalsloppet ${moment(this.datetime).format('YYYY')}`} />
          <meta name="og:url" content="/" />
          <meta name="description" content={`Lördagen den ${moment(this.datetime).format('D')} ${moment(this.datetime).format('MMMM')} kl 11.00 går starten för Sävedalsloppet ${moment(this.datetime).format('YYYY')}`} />
          <meta name="og:description" content={`Lördagen den ${moment(this.datetime).format('D')} ${moment(this.datetime).format('MMMM')} kl 11.00 går starten för Sävedalsloppet ${moment(this.datetime).format('YYYY')}`} />
          <meta name="og:image" content="/savedalsloppet_1906061847_o.jpg" />
          <meta name="og:image_type" content="image/jpeg" />
        </Helmet>
        <div className="right">
          <div className="image">
            <div
              style={{
                paddingBottom: `${1 / imageRight.aspectRatio * 100}%`,
                position: `relative`,
                width: `100%`,
                bottom: 0,
                left: 0,
                backgroundImage: `url(${imageRight.base64})`,
                backgroundSize: `cover`,
              }}
            />
            <img
              src={imageRight.src}
              srcSet={imageRight.srcSet}
              style={{
                width: `100%`,
                height: `100%`,
                margin: 0,
                verticalAlign: `middle`,
                objectFit: 'cover',
                position: 'absolute',
                top: 0,
                left: 0,
              }}
              sizes={imageRight.sizes}
            />
          </div>
        </div>
        <div className="left">
          <Page>
            <div className="front">
              <div className="logo">
                <img className="logo" src="/logo-highres.png" alt="Sävedalsloppet" />
              </div>
              {!this.state.ended ? (
                <section>
                  <h1>Lördagen den {moment(this.datetime).format('D')} {moment(this.datetime).format('MMMM')} kl {moment(this.datetime).format('HH:mm')}<br />går starten för Sävedalsloppet {moment(this.datetime).format('YYYY')}</h1>
                  <aside className="countdown with-time no-minutes" id="countdown">
                    {this.state.days ? (
                    <div className="countdown-item countdown-days">
                      <span className="countdown-caption countdown-caption-days">Dagar</span>
                      <span className="countdown-value countdown-value-days">
                        {this.state.days}
                      </span>
                    </div>
                    ):''}
                    {this.state.days > 0 || this.state.hours > 0 ? (
                    <div className="countdown-item countdown-hours">
                      <span className="countdown-caption countdown-caption-hours">Timmar</span>
                      <span className="countdown-value countdown-value-hours">
                        {this.state.hours}
                      </span>
                    </div>
                    ):''}
                    <div className="countdown-item countdown-minutes">
                      <span className="countdown-caption countdown-caption-minutes">Minuter</span>
                      <span className="countdown-value countdown-value-minutes">
                        {this.state.minutes}
                      </span>
                    </div>
                    <div className="countdown-item countdown-seconds">
                      <span className="countdown-caption countdown-caption-seconds">Sekunder</span>
                      <span className="countdown-value countdown-value-seconds">
                        {this.state.seconds}
                      </span>
                    </div>
                  </aside>
                  <div className="tc">
                    <p><Link to="/om-loppet">Se hela programmet</Link> med alla starttider, klasser, avgifter och banor.</p>
                  </div>
                  <div className="articles clearfix">
                    {this.props.data.races.edges.map(({ node: race }) => {
                      const responsiveSizes = race.frontmatter.image.childImageSharp.responsiveSizes || {};
                      return (
                        <article key={race.frontmatter.path}>
                          <div className="article" onClick={() => navigateTo(race.frontmatter.path)}>
                            <Link to={race.frontmatter.path}>
                              <div
                                className="img"
                                style={{
                                  paddingBottom: `${1 / responsiveSizes.aspectRatio * 100}%`,
                                  position: `relative`,
                                  width: `100%`,
                                  bottom: 0,
                                  left: 0,
                                  backgroundImage: `url(${responsiveSizes.base64})`,
                                  backgroundSize: `cover`,
                                }}
                              >
                                <img
                                  src={responsiveSizes.src}
                                  srcSet={responsiveSizes.srcSet}
                                  style={{
                                    width: `100%`,
                                    margin: 0,
                                    verticalAlign: `middle`,
                                    position: `absolute`,
                                  }}
                                  sizes={responsiveSizes.sizes}
                                />
                              </div>
                            </Link>
                            <h2><Link to={race.frontmatter.path}>{race.frontmatter.name}</Link></h2>
                            <p><Link to={race.frontmatter.path}>{race.frontmatter.description}</Link></p>
                          </div>
                        </article>
                      );
                    })}
                  </div>
                </section>
              ) : (
                <section>
                  <h1>Lördagen den 21 oktober<br />arrangerades Sävedalsloppet 2017</h1>
                  <div className="tc">
                    <p><Link to="/om-loppet">Visa resultatlistan</Link> med tider, klasser och banor.</p>
                  </div>
                  <div className="articles clearfix">
                    {this.props.data.races.edges.map(({ node: race }) => {
                      const responsiveSizes = race.frontmatter.image.childImageSharp.responsiveSizes || {};
                      return (
                        <article key={race.frontmatter.path}>
                          <div className="article" onClick={() => navigateTo(race.frontmatter.path)}>
                            <Link to={race.frontmatter.path}>
                              <div
                                className="img"
                                style={{
                                  paddingBottom: `${1 / responsiveSizes.aspectRatio * 100}%`,
                                  position: `relative`,
                                  width: `100%`,
                                  bottom: 0,
                                  left: 0,
                                  backgroundImage: `url(${responsiveSizes.base64})`,
                                  backgroundSize: `cover`,
                                }}
                              >
                                <img
                                  src={responsiveSizes.src}
                                  srcSet={responsiveSizes.srcSet}
                                  style={{
                                    width: `100%`,
                                    margin: 0,
                                    verticalAlign: `middle`,
                                    position: `absolute`,
                                  }}
                                  sizes={responsiveSizes.sizes}
                                />
                              </div>
                            </Link>
                            <h2><Link to={race.url}>{race.frontmatter.name}</Link></h2>
                            <p><Link to={race.url}>{race.frontmatter.description}</Link></p>
                          </div>
                        </article>
                      );
                    })}
                  </div>
                </section>
              )}
            </div>
          </Page>
        </div>
      </div>
    );
  }
}

export default IndexPage

export const pageQuery = graphql`
  query IndexQuery {
    imageRight: imageSharp(id: { regex: "/10km.jpg/" }) {
      responsiveSizes(
        toFormat: PNG
      ) {
        base64
        aspectRatio
        src
        srcSet
        sizes
        originalImg
        originalName
      }
    }
    races: allMarkdownRemark(
      filter: {id: { regex: "/\/om-loppet\/.+/" } }
      sort: { fields:[frontmatter___distance], order: DESC }
    ) {
      edges {
        node {
          frontmatter {
            name
            description
            path
            image {
            	childImageSharp {
                responsiveSizes(
                  maxWidth: 400
                  toFormat: PNG
                ) {
                  base64
                  aspectRatio
                  src
                  srcSet
                  sizes
                  originalImg
                  originalName
                }
              }
            }
          }
        }
      }
    }
  }
`
