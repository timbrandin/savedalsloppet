import React from 'react';
import Helmet from 'react-helmet';
import Page from '../components/Page';

class IndexPage extends React.PureComponent {
  render() {
    const imageRight = this.props.data.imageRight.responsiveSizes;
    return (
      <div className="container">
        <Helmet>
          <title>{`Startlista - Sävedalsloppet`}</title>
          <meta name="og:url" content="/startlista" />
          <meta name="description" content="Startlista för Sävedalsloppet 2017. I år har vi valt en extern partern för tidtagning." />
          <meta name="og:description" content="Startlista för Sävedalsloppet 2017. I år har vi valt en extern partern för tidtagning." />
          <meta name="og:image" content="/SavedalsloppetDSC_0346b.jpg" />
          <meta name="og:image_type" content="image/jpeg" />
        </Helmet>
        <div className="right">
          <div className="image">
            <div
              style={{
                paddingBottom: `${1 / imageRight.aspectRatio * 100}%`,
                position: `relative`,
                width: `100%`,
                bottom: 0,
                left: 0,
                backgroundImage: `url(${imageRight.base64})`,
                backgroundSize: `cover`,
              }}
            />
            <img
              src={imageRight.src}
              srcSet={imageRight.srcSet}
              style={{
                width: `100%`,
                height: `100%`,
                margin: 0,
                verticalAlign: `middle`,
                objectFit: 'cover',
                position: 'absolute',
                top: 0,
                left: 0,
              }}
              sizes={imageRight.sizes}
            />
          </div>
        </div>
        <div className="left">
          <Page>
            <div className="startlist">
              <h1>Startlista för Sävedalsloppet 2017</h1>
              <div className="lead tc">
                <p>I år har vi valt en extern partern för startlistan, du kommer dit genom länken här nedan.</p>
              </div>

              <section>
                <div className="articles clearfix">
                  <article style={{width: '100%'}}>
                    <div className="article super action">
                      <h3><a href="https://www.eqtiming.no/Live/Contestants?EventUID=36398" target="_blank">Öppna startlistan</a></h3>
                    </div>
                  </article>
                </div>
              </section>
            </div>
          </Page>
        </div>
      </div>
    );
  }
}

export default IndexPage

export const pageQuery = graphql`
  query StartlistQuery {
    imageRight: imageSharp(id: { regex: "/\/SavedalsloppetDSC_0346b.jpg/" }) {
      responsiveSizes(
        toFormat: PNG
      ) {
        base64
        aspectRatio
        src
        srcSet
        sizes
        originalImg
        originalName
      }
    }
  }
`
