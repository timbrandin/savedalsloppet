---
path: /aktuellt/3000m-hinder-for-motionarer
date: 2017-09-16T12:54:33.962+0200
title: 3000m hinder för motionärer
subtitle: Spring 3000m hinder på Vallhamra IP
page_title: 3000m hinder för motionärer
description: Spring 3000m hinder på Vallhamra IP för motionärer
image: /3000m-hinder.jpg
summary: I den nya klassen för motionärer, 3000m hinder springer deltagarna 8 varv, passerar 28 hinder och 7 vallgravar.
---
I Sävedalsloppet 2017 ingår också [3000m hinder för motionärer](/om-loppet/3000m-hinder), där löparna springer 8 varv runt Vallhamra Idrottsplats och på vägen passerar 28 hinder och 7 vallgravar. Hindrens höjd är 76.2 cm vilket är lika med damernas hinderhöjd. Vi vill understryka att loppet är främst riktat till motionärer som vill prova på något nytt.

Nu finns det dessutom ett hinder utsatt på bortre långsidan för dig som vill träna lite hinderteknik för loppet. Och den 12 oktober går det även att träna hinderteknik över vattengraven.

Banan går inne på Vallhamra Idrottsplats och startar i två heat kl 11.00 och 13:15.

Välkommen till Sävedalen och Sävedalsloppet den 21 oktober 2017.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
