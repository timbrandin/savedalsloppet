---
path: /aktuellt/5kr-gar-till-varldens-barn
date: 2017-09-06T18:32:25.962+0200
title: 5kr av startavgiften går till Världens Barn
subtitle: När du anmäler dig till Sävedalsloppet så går 5kr till Världens Barn
page_title: 5kr till Världens barn
description: När du anmäler dig till Sävedalsloppet så går 5kr till Världens Barn
image: /varldens-barn.jpg
summary: Som nytt för i år har vi tagit beslutet att ge bort 5kr av startavgiften till Världens Barn.
---
Nytt för i år är att Sävedalsloppet skänker bort 5kr utav startavgiften i alla klasser till Världens Barn som i en satsning för Sävedalens AIK att hjälpa till i samhället och bidra tillbaka. Samtidigt vill vi påminna att alla intäckter från loppet även om dom är små går raka vägen tillbaka till alla i klubben som drar nytta av detta vid tävlingar och annat som kostar för klubben.

Vi hoppas också detta är något som får ännu fler att vilja springa och bidra till både Världens Barn och alla barn i klubben. Välkommna till Sävedalen och Vallhamra idrottsplats den 21 oktober 2017.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
