---
path: /aktuellt/presentkort-fran-allum
date: 2017-08-17T08:58:33.962+0200
title: Presentkort från Allum
subtitle: Presentkort till dom tre första i både dam- och herrklassen.
page_title: Presentkort från Allum
description: Presentkort till dom tre första i både dam- och herrklassen.
image: /winners.jpeg
summary: Det kommer att delas ut presentkort från Allum till de tre första i både dam- och herrklassen.
---
Allum vår sponsor är med och bidrar till vinstpriserna och delar ut presentkort till de tre första i dam- och herrklassen.
