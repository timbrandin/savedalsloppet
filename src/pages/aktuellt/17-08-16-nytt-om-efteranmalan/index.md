---
path: /aktuellt/nytt-om-efteranmalan
date: 2017-08-17T19:21:33.962+0200
title: Nytt om efteranmälan
subtitle: Viktig information om efteranmälan
page_title: Nytt om efteranmälan
description: Viktig information om efteranmälan
image: /SavedalsloppetDSC_0371b.jpg
summary: Efteranmälan kan ske på anmälningssidan från den 16 okt till fre 20 okt.
---
Efteranmälan kan ske på anmälningssidan från tisdagen den 16 oktober till fredagen den 20 oktober kl 12.00. Anmälan kan därefter ske på tävlingsdagen från kl 9 i tävlingscentrum.

Extra avgift om 50kr/deltagare i ungdomsklasserna och 100 kr för övriga klasser. Betalning kan göras med kontant och numera även med Swish.
