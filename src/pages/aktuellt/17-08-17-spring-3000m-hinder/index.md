---
path: /aktuellt/spring-3000m-hinder
date: 2017-08-17T19:23:33.962+0200
title: Nytt för i år!
subtitle: Spring 3000m hinder på Vallhamra IP
page_title: Nytt för i år!
description: Spring 3000m hinder på Vallhamra IP
image: /3000m-hinder.jpg
summary: I den nya klassen, 3000m hinder springer deltagarna 8 varv, passerar 28 hinder och 7 vallgravar.
---
Nytt för i år är 3000m hinder, där löparna springer 8 varv runt Vallhamra Idrottsplats och på vägen passerar 28 hinder och 7 vallgravar. Hindrens höjd är 76.2 cm vilket är lika med damernas hinderhöjd.

Deltagande springer på egen risk. Vi hoppas det blir många som tycker det är kul att prova på hinder, för första gången eller för att prova på något annorlunda.

Och vi hoppas det blir ett riktigt kul lopp för alla deltagarna.

Banan går inne på Vallhamra Idrottsplats och startar i två heat kl 11.00 och 13:15.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
