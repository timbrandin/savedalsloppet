import React from 'react';
import Helmet from 'react-helmet';
import Page from '../components/Page';

class IndexPage extends React.PureComponent {
  render() {
    const imageRight = this.props.data.imageRight.responsiveSizes;
    return (
      <div className="container">
        <Helmet>
          <title>{`Resultat - Sävedalsloppet`}</title>
          <meta name="og:url" content="/resultat" />
          <meta name="description" content="Resultat för Sävedalsloppet 2017. I år har vi valt en extern partern för tidtagning." />
          <meta name="og:description" content="Resultat för Sävedalsloppet 2017. I år har vi valt en extern partern för tidtagning." />
          <meta name="og:image" content="/SavedalsloppetDSC_0461b.jpg" />
          <meta name="og:image_type" content="image/jpeg" />
        </Helmet>
        <div className="right">
          <div className="image">
            <div
              style={{
                paddingBottom: `${1 / imageRight.aspectRatio * 100}%`,
                position: `relative`,
                width: `100%`,
                bottom: 0,
                left: 0,
                backgroundImage: `url(${imageRight.base64})`,
                backgroundSize: `cover`,
              }}
            />
            <img
              src={imageRight.src}
              srcSet={imageRight.srcSet}
              style={{
                width: `100%`,
                height: `100%`,
                margin: 0,
                verticalAlign: `middle`,
                objectFit: 'cover',
                position: 'absolute',
                top: 0,
                left: 0,
              }}
              sizes={imageRight.sizes}
            />
          </div>
        </div>
        <div className="left">
          <Page>
            <div className="results">
              <h1>Resultatlista för Sävedalsloppet 2017</h1>
              <div className="lead tc">
                <p>I år har vi valt en extern partern för resultatlistan, du kommer dit genom länken här nedan.</p>
              </div>

              <section>
                <div className="articles clearfix">
                  <article style={{width: '100%'}}>
                    <div className="article super action">
                      <h3><a href="http://www.eqtiming.no/Result/?EventUID=36398" target="_blank">Öppna resultat</a></h3>
                    </div>
                  </article>
                </div>
              </section>

              <h3><a href="http://www.eqtiming.no/Result/?EventUID=23352" target="_blank">Resultat för 2016</a></h3>
              <h3><a href="http://www.eqtiming.no/Result/?EventUID=18700" target="_blank">Resultat för 2015</a></h3>
            </div>
          </Page>
        </div>
      </div>
    );
  }
}

export default IndexPage

export const pageQuery = graphql`
  query ResultQuery {
    imageRight: imageSharp(id: { regex: "/\/SavedalsloppetDSC_0461b.jpg/" }) {
      responsiveSizes(
        toFormat: PNG
      ) {
        base64
        aspectRatio
        src
        srcSet
        sizes
        originalImg
        originalName
      }
    }
  }
`
