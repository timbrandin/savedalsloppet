---
name: 10km
path: /om-loppet/10km
date: 2017-08-16T19:21:33.962+0200
title: Gör dig redo för nya rekord
subtitle: Spring 10km lätt kuperad terräng, med både asfalt och grus på Sävedalsloppet.
page_title: Gör dig redo för nya rekord | 10km
description: Spring 10km lätt kuperad terräng, med både asfalt och grus på Sävedalsloppet.
map: 10km
image: /10km.jpg
distance: 10000
classes:
  - name: "Kvinnor, K35, K40, K45, K50, K55"
    born: null
    price: 250
    start: "12:45"
  - name: "Män, M35, M40, M45, M50, M55"
    born: null
    price: 250
    start: "12:45"
---
Välkommen till Sävedalsloppets 10km-bana som arrangeras för 15:e gången med start och mål på Vallhamra IP.

En lätt och snabb bana som passar alla sorters löpare och som en gång i tiden var seedningslopp till Göteborgsvarvet.
Så varför inte uppleva glädjen med att springa snabbt runt det vackra Sävedalen.

Det kommer att delas ut presentkort från Allum till de tre första både i dam och herrklassen. Och för att vara på säkra sidan så har vi vätskekontroll efter 5 km, för det är alltid bra att fylla på ny energi eller vatten när det blir varmt väder.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
