---
name: 2.5km
path: /om-loppet/2-5km
date: 2017-08-16T19:21:33.962+0200
title: Utmana dig själv
subtitle: Spring 2.5km i lätt kuperad terräng, med mestadels asfalt i Sävedalsloppet.
page_title: Utmana dig själv | 2.5km
description: Spring 2.5km i lätt kuperad terräng, med mestadels asfalt i Sävedalsloppet.
map: 2.5km
image: /2-5km.jpg
distance: 2500
classes:
  - name: "F14, P14"
    born: "2003-2004"
    price: 120
    start: "12:10"
  - name: "F17, P17"
    born: "2000-2002"
    price: 120
    start: "12:10"
---
Välkommen till 2.5 km-banan som är en ny lättsprungen bana som går runt Sävedalen.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
