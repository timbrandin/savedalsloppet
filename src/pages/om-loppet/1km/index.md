---
name: 1km
path: /om-loppet/1km
date: 2017-08-16T19:21:33.962+0200
title: Utmana dig själv
subtitle: Spring 1km i lätt kuperad terräng, med mestadels asfalt i Sävedalsloppet.
page_title: Utmana dig själv | 1km
description: Spring 1km i lätt kuperad terräng, med mestadels asfalt i Sävedalsloppet.
map: 1km
image: /1km.jpg
distance: 1000
classes:
  - name: "F9"
    born: "2008-2010"
    price: 100
    start: "11:30"
  - name: "P9"
    born: "2008-2010"
    price: 100
    start: "11:40"
  - name: "F10, P10"
    born: "2007"
    price: 100
    start: "11:50"
  - name: "F12, P12"
    born: "2005-2006"
    price: 100
    start: "12:00"
---
För dig mellan 7 och 12 år gäller sträckan 1 km och som går runt Vallhamra.
Det är roligt att ha en nummerlapp på sig, ställa sig på startlinjen och ge sig iväg till publikens jubel.

När ni kommer i mål blir det medalj, vätska och banan. De som är snabbast kommer att få ställa sig på prispallen.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
