---
name: 5km
path: /om-loppet/5km
date: 2017-08-16T19:21:33.962+0200
title: Utmana dig själv
subtitle: Spring 5km lätt kuperad terräng, med mestadels asfalt i Sävedalsloppet.
page_title: Utmana dig själv | 5km
description: Spring 5km lätt kuperad terräng, med mestadels asfalt i Sävedalsloppet.
map: 5km
image: /5km.jpg
distance: 5000
classes:
  - name: "Kvinnor, K60, K65, K70, K75, K80"
    born: null
    price: 200
    start: "12:30"
  - name: "Män, M60, M65, M70, M75, M80"
    born: null
    price: 200
    start: "12:30"
---
Välkommen till Sävedalsloppets 5 km-bana som är en snabb och fina bana som går runt vackra Sävedalen.

Särskilda klasser för seniorer, 60 år, 65 år, 70 år och 80 år.

För er som vill som testa formen mot framtidens löpare har ni chans här, där alla klasser startar samtidigt.

De tre bästa i varje klass får en särskild glasplakett och alla får medalj.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
