---
name: Knatteklassen
path: /om-loppet/knatteklassen
date: 2017-08-16T19:21:33.962+0200
title: Ta med hela familjen
page_title: Ta med hela familjen | Knatteklassen
subtitle: Ett lopp för de minsta, ha det kul med hela familjen och spring Sävedalsloppet tillsammans.
description: Ett lopp för de minsta, ha det kul med hela familjen och spring Sävedalsloppet tillsammans.
image: /knatteklassen.jpg
distance: 250
classes:
  - name: "Knatteklassen"
    born: "2011-2015"
    price: 50
    start: "11:00-13:00"
---
Knattebanans 250m är ett lopp för barn upp till 6 år. I detta lopp registreras inga tider utan loppet är enbart ett fun-run.

Självklart är även äldre barn som inte vill tävla, välkomna att delta på Knatteloppet. Anmälan  vid startplatsen för knattebanan. Knattebanan går på gräsplanen för fotboll.

Banan går inne på Vallhamra Idrottsplats för Knatteloppet, och mamma och pappa får självklart gärna springa med.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
