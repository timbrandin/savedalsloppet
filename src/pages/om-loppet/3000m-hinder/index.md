---
name: 3000m Hinder
path: /om-loppet/3000m-hinder
date: 2017-08-17T19:21:33.962+0200
title: Prova hinder på Vallhamra
subtitle: Spring 3000m hinder på Vallhamra IP. På 8 varv passerar ni 28 hinder och 7 vallgravar.
page_title: Prova hinder på Vallhamra IP | 3000m hinder
description: Spring 3000m hinder på Vallhamra IP. På 8 varv passerar ni 28 hinder och 7 vallgravar.
image: /3000m-hinder.jpg
distance: 3000
classes:
  - name: "Kvinnor"
    born: null
    price: 150
    start: "11:00"
  - name: "Män"
    born: null
    price: 150
    start: "11:00"
---
Nu har du möjlighet att springa 3000m hinder på Vallhamra IP under Sävedalsloppet. Det blir nästan 8 varv och du passerar 28 hinder och 7 vattengravar. Hindrens höjd är 76.2 cm vilket är lika med damernas hinderhöjd. Manuell tidtagning kommer att tillämpas.

Heat 1 startar kl 11.00, heat 2 startar kl 13:15 och därefter varje 20:e minut. Löparna deltager på egen risk.

Vi vill understryka att loppet är främst riktat till motionärer som vill prova på något nytt.

Mer information om loppen finner ni på vår sida [Om loppet, PM](/om-loppet).
