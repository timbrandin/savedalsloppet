import React from 'react';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';
import Page from '../components/Page';
import SubMenu from '../components/SubMenu';
import MapCanvas from '../components/MapCanvas';
import Table from '../components/Table';

const About = ({ data }) =>
  <div className="container">
    <Helmet>
      <title>{`Om loppet - Sävedalsloppet`}</title>
      <meta name="og:url" content="/om-loppet" />
      <meta name="description" content="Lördagen 21 oktober 2017. Spring, eller gå höstens höjdpunkt Sävedalsloppet." />
      <meta name="og:description" content="Lördagen 21 oktober 2017. Spring, eller gå höstens höjdpunkt Sävedalsloppet." />
      <meta name="og:image" content="/SavedalsloppetDSC_0379b.jpg" />
      <meta name="og:image_type" content="image/jpeg" />
    </Helmet>
    <div className="right">
      <MapCanvas selected="10km" />
    </div>
    <div className="left">
      <Page>
        <SubMenu />
        <div className="about">
          <h1>Lördagen den 21 oktober</h1>
          <h2>Spring eller gå höstens höjdpunkt Sävedalsloppet</h2>
          <div className="cols">
            <p>
              <strong>Anmälan</strong>
              Senast måndagen den 16 oktober 2017 här på vår hemsida, <Link to="/anmalan">anmäl dig här</Link>. Så fort startavgiften är inbetalad är anmälan giltig. Det går också att anmäla sig via bankgiro: 753-3052 Sävedalens AIK. Ange namn, e-post, födelseår, adress, klass och klubb/företag.
            </p>
            <p>
              <strong>Efteranmälan</strong>
              Efteranmälan kan ske på anmälningssidan från tisdagen den 17 oktober till fredagen den 20 oktober kl 12.00. Anmälan kan därefter ske på tävlingsdagen från kl 9 i tävlingscentrum. Extra avgift om 50kr/deltagare i ungdomsklasserna och 100 kr för övriga klasser. Swish eller kontantbetalning.
            </p>
            <p>
              <strong>Tävlingscentrum, start och mål</strong>
              Vallhamra idrottsplats.
            </p>
            <p>
              <strong>Banorna</strong>
              Till största delen på asfalt och i området söder om Vallhamra idrottsplats.
            </p>
            <p>
              <strong>Spring 3000 m hinder på Vallhamra IP</strong>
              Nu har du möjlighet att springa 3000m hinder på Vallhamra IP under Sävedalsloppet. Det blir nästan 8 varv och du passerar 28 hinder och 7 vattengravar. Hindrens höjd är 76,2 cm vilket är lika med damernas hinderhöjd. Manuell tidtagning kommer att tillämpas. De tävlande springer på egen risk. Vi vill understryka att loppet är främst riktat till motionärer som vill prova på något nytt.
            </p>
            <p>
              <strong>Knatteklass</strong>
              Ingen föranmälan – anmälan och betalning vid startplatsen på tävlingsdagen. Individuell start. Pris till alla.
            </p>
            <p>
              <strong>Nummerlappar</strong>
              Hämtas på tävlingsexpeditionen, Vallhamra Sportcenter från 09.00.
            </p>
            <p>
              <strong>Omklädning</strong>
              Omklädningsmöjligheter finns i Vallhamra Sportcenter.
            </p>
            <p>
              <strong>Vätskekontroll (10 km)</strong>
              Vid 5 kilometer och i mål – saft och vatten.
            </p>
            <p>
              <strong>Priser</strong>
              Till de främsta i tävlingsklasserna, utlottade priser i motionsklasser och medaljer till alla.
            </p>
            <p>
              <strong>Start och resultatlista</strong>
            </p>
            <ul>
              <li>Här <Link to="/startlista">hittar du startlistan för årets evenemang.</Link></li>
              <li>Och här <Link to="/resultat">hittar du resultatet för årets evenemang.</Link></li>
            </ul>
            <p>
              <a href="https://www.google.se/maps/place/Idrottsplats+Vallhamra/@57.724374,12.078076,17z/data=!3m1!4b1!4m2!3m1!1s0x464ff6a1a6301ba3:0x53dd1404952fbc02?hl=sv" target="_blank"><strong>Hitta hit med karta</strong></a>
              Vallhamra Idrottsplats<br />
              Hultvägen<br />
              433 64 Sävedalen
            </p>
            <p>
              <strong>Parkering</strong>
              Anvisade parkeringsplatser vid eller i närheten av Vallhamra idrottsplats. <a href="http://reseplanerare.vasttrafik.se" target="_blank">Samåk gärna eller åk kommunalt</a>. De flesta parkeringarna syns även på kartan i vitt här på hemsidan.
            </p>
            <p>
              <a href="http://reseplanerare.vasttrafik.se/bin/query.exe/sn?L=vs_vasttrafik&ld=fe1&L=vs_vasttrafik&REQ0JourneyStopsS0A=255&REQ0JourneyStopsS0G=Centralstationen,%20G%C3%B6teborg&REQ0JourneyStopsZ0A=255&REQ0JourneyStopsZ0G=Vallhamra%20torg,%20Partille&start=yes&REQ0JourneyDate=2014-10-18&REQ0JourneyTime=11:00&REQ0HafasSearchForw=0" target="_blank"><strong>Ta dig hit från Göteborg</strong></a>
              Svart Express, Centralstationen Göteborg – Vallhamra Torg, gå 600 m öster ut längs Ugglumsleden.
            </p>
            <p>
              <a href="http://reseplanerare.vasttrafik.se/bin/query.exe/sn?L=vs_vasttrafik&ld=fe1&L=vs_vasttrafik&REQ0JourneyStopsS0A=255&REQ0JourneyStopsS0G=Partille%20Centrum,%20Partille&REQ0JourneyStopsZ0A=255&REQ0JourneyStopsZ0G=Bockemossen,%20Partille&start=yes&REQ0JourneyDate=2014-10-18&REQ0JourneyTime=11:00&REQ0HafasSearchForw=0" target="_blank"><strong>Ta dig hit från Partille</strong></a>
              Linje 515, Partille Centrum – Bockemossen, gå 100m
            </p>
          </div>

          {data && data.races && (
            <Table rows={data.races.edges} />
          )}
        </div>
      </Page>
    </div>
  </div>

export default About;

export const pageQuery = graphql`
  query RaceQuery {
    races: allMarkdownRemark(
      filter: { id: { regex: "/\/om-loppet\//" } }
      sort: { order: ASC, fields: [frontmatter___distance] }
    ) {
      edges {
        node {
          id
          frontmatter {
            distance
            classes {
              name
              born
              price
              start
            }
          }
        }
      }
    }
  }
`;
