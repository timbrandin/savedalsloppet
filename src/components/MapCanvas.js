import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import geoXML from '../utils/geoXML';

class MapCanvas extends React.Component {
  static propTypes = {
    selected: PropTypes.string,
  };

  constructor(props) {
    super(props);
    const { selected } = this.props;
    this.state = {
      selected,
      apiReady: false,
    };
  }

  componentDidMount() {
    if (typeof window !== 'undefined' && window.google) {
      this.handleOnLoad();
    }
  }

  componentWillReceiveProps({ selected }) {
    if (this.props.selected !== selected) {
      this.setState({ selected });
      this.clearMap();
      this.loadKML('/savedalsloppet-' + selected + '.kml');
    }
  }

  handleOnLoad() {
    this.setState({ apiReady: true });
    this.initMap();
    this.clearMap();
    this.loadKML('/savedalsloppet-' + this.state.selected + '.kml');
  }

  handleChange(value) {
    this.clearMap();
    this.loadKML('/savedalsloppet-' + value + '.kml');
    this.setState({
      selected: value,
    });
  }

  render() {
    return (
      <div className="map-canvas">
        {typeof window !== 'undefined' && !window.google && (
          <Helmet
            script={[{ src: 'https://maps.googleapis.com/maps/api/js?libraries=visualization&key=AIzaSyBlUWNrvl8HBj79edhMDIFpKZ5fFMXC4yM' }]}
            onChangeClientState={(_, { scriptTags }) => {
              if (scriptTags) {
                const scriptTag = scriptTags[0];
                scriptTag.onload = (...args) => this.handleOnLoad(...args);
              }
            }}
          />
        )}
        <div className="map-header clearfix">
          <h3>Våra banlängder</h3>
          {[
            '10km',
            '5km',
            '2.5km',
            '1km',
          ].map(option =>
            <span key={option}>
              <input
                type="radio"
                value={option}
                name="track"
                id={option}
                onChange={() => this.handleChange(option)}
                checked={this.state.selected === option}
              />
              <label htmlFor={option}>{option}</label>
            </span>
          )}
          <img className="close" src="/close-circled.png" />
        </div>
        <div id="map_canvas"></div>
      </div>
    )
  }

  loadKML(url) {
    geoXML.fetchXML(url, (responseXML) => {
      var placemarkNodes = responseXML.getElementsByTagName('Placemark');
      for (let i = 0; i < placemarkNodes.length; i++) {
        var linestringNode = placemarkNodes[i].getElementsByTagName('LineString');
        this.parseLineString(linestringNode);
      }
    });
  }

  parseLineString(doc) {
    // Extract the coordinates
    const coords = geoXML.nodeValue(doc[0].getElementsByTagName('coordinates')[0]).trim();
    const coordString = coords.replace(/\s+/g, ' ').replace(/, /g, ',');
    const pathCoords = coordString.split(' ');
    const lineCoordinates = [];
    let startCoord, goalCoord;
    let i = 0;
    var R = 6371; // km
    let lat0, lon0, lat1, lon1, dLat = 0, dLon = 0, distance = 0, km = 0, bearing = 0;

    pathCoords.forEach(coord => {
      let newKm = false;

      // Polygons/lines not supported in v3, so only plot markers
      const pointCoords = coord.split(',');

      const point = {
        lat: parseFloat(pointCoords[1]),
        lng: parseFloat(pointCoords[0]),
        alt: parseFloat(pointCoords[2])
      };

      // Calculate distance between coords.
      const lat2 = parseFloat(pointCoords[1]);
      const lon2 = parseFloat(pointCoords[0]);
      if (lat1) {
        var dLat = (lat2 - lat1).toRad();
        var dLon = (lon2 - lon1).toRad();

        // Haversine.
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1.toRad()) * Math.cos(lat2.toRad());
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var delta = R * c;
        distance += delta;

        // dLon = (lon2 - lon0).toRad();
        // // Bearing.
        // var y = Math.sin(dLon) * Math.cos(lat2.toRad());
        // var x = Math.cos(lat0.toRad())*Math.sin(lat2.toRad()) -
        //         Math.sin(lat0.toRad())*Math.cos(lat2.toRad())*Math.cos(dLon);
        // bearing = Math.atan2(y, x).toDeg();
        //
        // // Normalize bearing to compass.
        // bearing = (bearing + 360) % 360;
      }
      lat1 = lat2;
      lon1 = lon2;

      if (Math.floor(distance) > km && km <= 19) {
        newKm = true;
        km = Math.floor(distance);

        this.placeMarker({
          coord: new google.maps.LatLng(lat2, lon2),
          url: '/' + km + 'km.png',
          x: 16,
          y: 32,
          retina: true
        });
      }

      if (newKm) {
        lat0 = lat2;
        lon0 = lon2;
      }
      if (!lat0) {
        lat0 = lat2;
        lon0 = lon2;
      }

      // Get the start and goal coord.
      if (!startCoord) {
        startCoord = new google.maps.LatLng(point.lat, point.lng);
      }
      goalCoord = new google.maps.LatLng(point.lat, point.lng);

      // Add each coord to the line.
      lineCoordinates.push(new google.maps.LatLng(point.lat, point.lng));
    });

    // Define a symbol.
    const lineSymbol = {
      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
      fillColor: '#a1a1a1',
      fillOpacity: 0.75,
      scale: 2
    };

    // Draw the line on the map.
    const line = new google.maps.Polyline({
      path: lineCoordinates,
      geodesic: true,
      strokeColor: '#a1a1a1',
      strokeWeight: 4,
      strokeOpacity: 1,
      icons: [{
        icon: lineSymbol,
        offset: '40px',
        repeat: '100px',
      }],
      map: this.map
    });

    // Add marker to the list of all markers.
    this.polylines.push(line);

    this.placeMarker({
      coord: startCoord,
      url: '/start.png',
      x: 9,
      y: 25,
      retina: true,
      zIndex: 1
    });

    this.placeMarker({
      coord: goalCoord,
      url: '/goal.png',
      x: 9,
      y: 0,
      retina: true,
      zIndex: 1
    });
  }

  placeMarker(options) {
    // Wait for image to load.
    const img = document.createElement('img');
    console.log(img);
    img.onload = (args) => {
      console.log(args);
      const { path, srcElement } = args;
      let height;
      let width;
      if (path && path.length > 0 && path[0].width && path[0].height) {
        width = path[0].width;
        height = path[0].height;
      } else if (srcElement && srcElement.width && srcElement.height) {
        width = srcElement.width;
        height = srcElement.height;
      } else {
        return;
      }
      // Define goal marker image.
      const image = {
        url: options.url,
        // This marker is 50 pixels wide by 25 pixels tall.
        size: new google.maps.Size(width, height),
        // The origin for this image is 0,0.
        origin: new google.maps.Point(0,0),
        // The anchor for this image is the base of the flagpole at 0,32.
        anchor: new google.maps.Point(options.x, options.y),
      };

      if (options.retina) {
        image.scaledSize = new google.maps.Size(width / 2, height / 2);
      }

      // Draw the goal on the map.
      const marker = new google.maps.Marker({
        position: options.coord,
        map: this.map,
        icon: image,
        zIndex: options.zIndex || 0,
      });

      // Add marker to the list of all markers.
      this.markers.push(marker);
    };
    img.src = options.url;
  }

  clearMap() {
    // Clear all markers.
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
    // Clear all polylines.
    for (var i = 0; i < this.polylines.length; i++) {
      this.polylines[i].setMap(null);
    }
    this.polylines = [];
  }

  initMap() {
    // Create an array of styles.
    this.markers = [];
    this.polylines = [];

    var styles = [
      {
        "elementType": "labels",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
          { "color": "#ffffff" }
        ]
      },{
        "featureType": "poi.attraction",
        "stylers": [
          { "visibility": "on" },
          { "lightness": 10 },
          { "color": "#8ada7c" }
        ]
      },{
        "featureType": "poi.business",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "poi.government",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "poi.medical",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "poi.park",
        "stylers": [
          { "visibility": "on" },
          { "color": "#8ada7c" }
        ]
      },{
        "featureType": "poi.place_of_worship",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "poi.school",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "poi.sports_complex",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "water",
        "stylers": [
          { "visibility": "simplified" },
          { "hue": "#006eff" },
          { "lightness": -35 }
        ]
      },{
        "featureType": "transit",
        "stylers": [
          { "visibility": "on" }
        ]
      },
      {
        "featureType": "landscape.natural",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
          { "visibility": "on" }
        ]
      },{
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
          { "visibility": "on" },
          { "color": "#ececec" }
        ]
      },{
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          { "weight": 3.1 }
        ]
      },{
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
          { "visibility": "off" }
        ]
      },{
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
          { "visibility": "on" }
        ]
      },{
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
          { "visibility": "on" },
          { "hue": "#00ff4d" }
        ]
      },{
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
          { "visibility": "on" }
        ]
      }
    ];

    // Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    const styledMap = new google.maps.StyledMapType(styles,
      {name: "Sävedalsloppet"});

    const mapOptions = {
      zoom: 14,
      center: new google.maps.LatLng(57.7222431, 12.0810332),
      optimized: true,
      streetViewControl: false,
      panControl: false,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
      }
    };

    this.map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);

    // Associate the styled map with the MapTypeId and set it to display.
    this.map.mapTypes.set('map_style', styledMap);
    this.map.setMapTypeId('map_style');

    if (this.map && document.body.offsetWidth < 1100) {
      this.map.setOptions({
        draggable: true,
        scrollwheel: true,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        navigationControl: true,
        overviewMapControl: false,
        disableDoubleClickZoom: true
      });
    }

    google.maps.event.addDomListener(window, 'resize', () => {
      this.map.setCenter(new google.maps.LatLng(57.7221415, 12.0789806));
    });

    // On re-renders we want to show the map once again, this can first be done
    // after it is ready which happens just after we created it.
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
      this.map.setCenter(new google.maps.LatLng(57.7221415, 12.0789806));
    }, 0);
  }
}

/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toDeg) === "undefined") {
  Number.prototype.toDeg = function() {
    return this * 180 / Math.PI;
  }
}

/** Rounds numbers to a certain level of detail. */
if (typeof(Number.prototype.round) === "undefined") {
  Number.prototype.round = function(decimals) {
    return Math.round(this * decimals * 10) / (decimals * 10);
  }
}

export default MapCanvas;
