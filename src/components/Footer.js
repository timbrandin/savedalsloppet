import React from 'react';

const Footer = () =>
  <footer>
    <div className="sponsors">
      <p className="tc">
        Tack till våra fina samarbetspartners.
      </p>
      <br />
      <img className="ica" src="/ica.png" alt="Ica Supermarket Vallhamra" />
      <img className="partillebo" src="/partillebo.png" alt="Partillebo" />
      <a href="http://www.positivum.se/"><img className="loposko" src="/positivum.png" alt="Positivum" /></a>
      <a href="http://www.lopsko.se"><img className="loposko" src="/loposko.png" alt="Löp &amp; Sko" /></a>
      <a href="https://studiointeract.se"><img className="loposko" src="/studiointeract.png" alt="Studio Interact: Webbyrå i Göteborg" /></a>
      <img className="allum" src="/allum.png" alt="Allum" />
      {/*<a href="http://nordicwellness.se"><img className="nordicwellness" src="/nordic-wellness-logo.png" alt="Nordic Wellness" /></a>*/}
    </div>
    <div className="red">
      <div className="inner clearfix">
        <dl>
          <a href="http://saik.nu" className="saik" title="Sävedalens AIK">
            <img src="/saik.png" alt="Sävedalens AIK" />
          </a>
        </dl>
        <dl>
          <dd>Sävedalens AIK:s kansli</dd>
          <dd>Box 2020 433 03 Sävedalen</dd>
          <dd>Telefon: <a href="tel:+4631268066">031 - 26 80 66</a></dd><br />
          <dd><a href="mailto:savedalsloppet@saik.nu">
                savedalsloppet@saik.nu</a></dd>
        </dl>
      </div>
    </div>
  </footer>

export default Footer;
