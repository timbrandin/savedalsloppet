import React from 'react';

class Table extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: true
    };
  }

  componentDidMount() {
    this.resize = () => {
      this.setState({
        isMobile: window.screen.availWidth < 640
      });
    };
    window.addEventListener('resize', this.resize());
    this.resize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  render() {
    const {
      rows = []
    } = this.props;
    if (rows.length === 0) {
      return null;
    }
    return (
      <table className="table">
        <thead>
          <tr>
            <th>
              Banlängd
            </th>
            <th>
              Klasser
            </th>
            <th>
              <span className="hide-mobile">
                Född
              </span>
            </th>
            <th className="tr">
              Avgift
            </th>
            <th>
              Starttid
            </th>
          </tr>
        </thead>
        {rows.map(({
          node: {
            id,
            frontmatter
          }
        }, index) => {
          if (frontmatter.classes && frontmatter.classes.length === 0) {
            return null;
          }
          return (
            <tbody key={id}>
              {frontmatter.classes.map(row => <tr key={row.name}>
                {frontmatter.distance !== 3000 ? (
                  <td>
                    {frontmatter.distance >= 1000
                      ? (
                        <span>{frontmatter.distance / 1000} km</span>
                      )
                      : (
                        <span>{frontmatter.distance} m</span>
                      )}
                  </td>
                ) : (
                  <td>
                    3000 m hinder
                  </td>
                )}
                <td>
                  {row.name}
                </td>
                <td>
                  <span className="hide-mobile">
                    {row.born}
                  </span>
                </td>
                <td className="tr">
                  {row.price} kr
                </td>
                <td>
                  {row.start}
                </td>
              </tr>)}
            </tbody>
          );
        })}
      </table>
    );
  }
}

export default Table;
