import React from 'react';
import Link from 'gatsby-link';

const links = [{
  url: '/om-loppet/10km',
  title: '10km'
}, {
  url: '/om-loppet/5km',
  title: '5km'
}, {
  url: '/om-loppet/2-5km',
  title: '2.5km'
}, {
  url: '/om-loppet/1km',
  title: '1km'
}, {
  url: '/om-loppet/knatteklassen',
  title: 'Knatteklassen'
}, {
  url: '/om-loppet/3000m-hinder',
  title: '3000m hinder'
}];

class SubMenu extends React.PureComponent {
  render() {
    return (
      <div className="submenu">
        <ul>
          {links.map(link =>
            <li key={link.url}>
              <Link activeClassName="active" to={link.url}>{link.title}</Link>
            </li>
          )}
        </ul>
      </div>
    );
  }
}

export default SubMenu;
