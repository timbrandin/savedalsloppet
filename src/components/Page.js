import React from 'react';
import Footer from './Footer';

const Page = ({ children }) =>
  <div className="container">
    <div className="page-row page-row-expanded">
      <div className="inner">
        {children}
      </div>
    </div>

    <div className="page-row">
      <Footer />
    </div>
  </div>

export default Page;
