import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';

import './index.css'

const links = [
  {
    url: '/',
    name: 'Hem'
  },
  {
    url: '/om-loppet',
    name: 'Om loppet, PM'
  },
  {
    url: '/anmalan',
    name: 'Anmälan'
  },
  {
    url: '/startlista',
    name: 'Startlista'
  },
  {
    url: '/resultat',
    name: 'Resultat'
  },
  {
    url: '/aktuellt',
    name: 'Aktuellt (5)'
  },
  {
    url: '/kontakt',
    name: 'Kontakt'
  }
];

class Header extends React.Component {
  componentDidMount() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.platform);
    if (iOS) {
      document.body.classList.add('ios');
    }

    const resizeHandler = () => {
      var mobile = 970;
      var width = document.body.offsetWidth;
      document.body.classList.toggle('mobile', width < mobile);
      document.body.classList.toggle('desktop', width >= mobile);
    };
    if (typeof window !== 'undefined') {
      window.addEventListener('resize', resizeHandler);
    }
    resizeHandler();
    document.body.classList.add('ready');
    document.body.classList.add('mounted');
  }

  render() {
    return (
      <header className="navigation">
        <Link
          to="/"
          onClick={() => document.body.classList.toggle('nav-visible', false)}
          className="site-logo logo"
        ><img src="/logo.png" alt="Sävedalsloppet" /></Link>

        <nav className="navbar" role="navigation">
          <div
            className="nav-button"
            onClick={() => document.body.classList.toggle('nav-visible')}
          >
            <p>Meny</p>
            <div className="stripes">
              <span className="stripe1"></span>
              <span className="stripe2"></span>
              <span className="stripe3"></span>
            </div>
          </div>
          <div className="desktop-nav">
            <ul>
              {links.map(link =>
                <li key={'desktop-' + link.url}>
                  <Link
                    {...link.url === '/' && { exact: true }}
                    activeClassName="active"
                    to={link.url}
                  >{link.name}</Link>
                </li>
              )}
            </ul>
          </div>
        </nav>
        <div className="mobile-nav">
          <div className="region">
            <ul>
              {links.map(link =>
                <li key={'mobile-' + link.url}>
                  <Link
                    exact
                    activeClassName="active"
                    to={link.url}
                    onClick={() => document.body.classList.toggle('nav-visible', false)}
                  >{link.name}</Link>
                </li>
              )}
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

const TemplateWrapper = ({ children }) =>
  <div>
    <Helmet>
      <title>Sävedalsloppet</title>
      <meta name="description" content="Lördagen den 21 oktober kl 11:00 går första starten för Sävedalsloppet 2017." />
      <meta name="keywords" content="sävedalen, tävling, löpning, partille, spring" />
      <meta name="author" content="Sävedalsloppet" />
      <meta name="og:site_name" content="Sävedalsloppet" />
      {/* <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" /> */}
      <link rel="alternate" hrefLang="sv" href="https://savedalsloppet.se" />
    </Helmet>
    <Header />
    <div className="main">
      {children()}
    </div>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Anton|Droid+Serif:400,700|Roboto&subset=latin-ext,latin" />
  </div>

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper;
