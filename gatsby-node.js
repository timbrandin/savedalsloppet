const path = require('path');
const ImageSharp = require('gatsby-transformer-sharp/extend-node-type');
const {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLBoolean,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLEnumType,
} = require(`graphql`);

exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators;

  const blogPostTemplate = path.resolve(`src/templates/blog-post.js`);
  const racePageTemplate = path.resolve(`src/templates/race-page.js`);

  graphql(`{
    allMarkdownRemark(
      filter: { id: { regex: "/\/aktuellt\//" } }
      sort: { order: DESC, fields: [frontmatter___date] }
      limit: 1000
    ) {
      edges {
        node {
          excerpt(pruneLength: 250)
          html
          id
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            path
            title
          }
        }
      }
    }
  }`).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors);
    }

    result.data.allMarkdownRemark.edges
      .forEach(({ node }) => {
        if (/om\-loppet\/[^/]+/.test(node.frontmatter.path)) {
          component = racePageTemplate;
        }
        createPage({
          path: node.frontmatter.path,
          component: blogPostTemplate,
          context: {} // additional data can be passed via context
        });
      });
  });

  graphql(`{
    allMarkdownRemark(
      filter: { id: { regex: "/\/om\-loppet\//" } }
      sort: { order: DESC, fields: [frontmatter___date] }
      limit: 1000
    ) {
      edges {
        node {
          excerpt(pruneLength: 250)
          html
          id
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            path
            title
          }
        }
      }
    }
  }`).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors);
    }

    result.data.allMarkdownRemark.edges
      .forEach(({ node }) => {
        createPage({
          path: node.frontmatter.path,
          component: racePageTemplate,
          context: {} // additional data can be passed via context
        });
      });
  });
}

exports.onCreateNode = ({ node, boundActionCreators, getNode }) => {
  // const { createNodeField } = boundActionCreators

  if (node.internal.type === `MarkdownRemark`) {
    // console.log(node.fileAbsolutePath, node.frontmatter.image);
    let file = path.join(path.dirname(node.fileAbsolutePath), node.frontmatter.image, ' absPath of file');
    if (path.isAbsolute(node.frontmatter.image)) {
      file = path.join(process.cwd(), '/src/images', node.frontmatter.image.trim());
    }
    node.frontmatter.image = `${file} absPath of file`;
  }
};
